import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'mo-angular-app';
  open = false;
  toggle() {
    this.open = !this.open;
  }
}
