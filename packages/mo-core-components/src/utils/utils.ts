export function format(first: string, middle: string, last: string): string {
  return (
    (first || '') + (middle ? ` ${middle}` : '') + (last ? ` ${last}` : '')
  );
}

export function generateId(): string {
  return Math.random().toString(36).slice(2, 12);
}
