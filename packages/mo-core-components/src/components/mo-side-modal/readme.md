# mo-side-modal



<!-- Auto Generated Below -->


## Properties

| Property               | Attribute                | Description                                                             | Type                | Default   |
| ---------------------- | ------------------------ | ----------------------------------------------------------------------- | ------------------- | --------- |
| `allowBackgroundClose` | `allow-background-close` | Determines whether the modal can be closed by clicking outside of it.   | `boolean`           | `true`    |
| `customClass`          | `custom-class`           | A custom CSS class for styling the element.                             | `string`            | `''`      |
| `isOpen`               | `is-open`                | Determines if the side modal is open or closed.                         | `boolean`           | `false`   |
| `slideDirection`       | `slide-direction`        | Prop that sets the direction of the slide animation for the side modal. | `"left" \| "right"` | `'right'` |


## Events

| Event        | Description                             | Type               |
| ------------ | --------------------------------------- | ------------------ |
| `closeModal` | Event emitted when the modal is closed. | `CustomEvent<any>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
