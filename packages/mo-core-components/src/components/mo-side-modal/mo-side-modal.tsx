import { Component, h, Prop, Event, Watch, State } from '@stencil/core';
import { EventEmitter } from 'stream';
import { SlideDirection } from './constants/mo-side-modal.constants';
@Component({
  tag: 'mo-side-modal',
  styleUrl: 'mo-side-modal.scss',
  shadow: false,
})
export class MoSideModal {
  /**
   * A custom CSS class for styling the element.
   */
  @Prop() customClass: string = '';
  /**
   * Determines if the side modal is open or closed.
   */
  @Prop() isOpen: boolean = false;
  /**
   * Determines whether the modal can be closed by clicking outside of it.
   */
  @Prop() allowBackgroundClose: boolean = true;
  /**
   * Prop that sets the direction of the slide animation for the side modal.
   * @default 'right'
   */
  @Prop() slideDirection: SlideDirection = 'right';
  /**
   * Indicates if modal is closing.
   */
  @State() isClosing: boolean = false;
  /**
   * Indicates if modal is opened.
   */
  @State() openModal: boolean = false;
  /**
   * Event emitted when the modal is closed.
   */
  @Event() closeModal: EventEmitter;

  onClick = (event, isBackground?: boolean) => {
    if (isBackground && !this.allowBackgroundClose) return;
    this.isClosing = true;
    this.closeModal.emit(event);
    setTimeout(() => {
      this.isClosing = false;
    }, 900);
  };

  @Watch('isOpen')
  modalToggled(newValue: boolean) {
    if (newValue) {
      this.openModal = true;
    } else {
      setTimeout(() => {
        this.openModal = false;
      }, 300);
    }
  }

  render() {
    const classes = {
      'mo-side-modal': true,
      'mo-side-modal__opened': this.openModal,
      'mo-side-modal__closed': this.isClosing,
    };

    return (
      <div class={classes}>
        <div
          class={`overlay ${this.isClosing ? 'close-in-progress' : ''}`}
          onClick={(event) => this.onClick(event, true)}
        ></div>
        <div
          class={`modal ${
            this.isClosing ? 'close-in-progress-' + this.slideDirection : ''
          } ${this.slideDirection}`}
        >
          <div
            class="modal__close-icon"
            onClick={(event) => this.onClick(event, false)}
          >
            <slot name="modal-close-icon"></slot>
          </div>
          <div class="modal__content">
            <slot name="modal-content"></slot>
          </div>
        </div>
      </div>
    );
  }
}
