import { Component, h, Prop } from '@stencil/core';

@Component({
  tag: 'mo-dummy-button',
  styleUrl: 'mo-dummy-button.scss',
  shadow: true,
})
export class MoDummyButton {
  /**
   * The buttons label
   */
  @Prop() label: string;

  private getText(): string {
    return this.label || '';
  }

  render() {
    return <button>{this.getText()}</button>;
  }
}
