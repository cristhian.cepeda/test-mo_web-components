# mo-dummy-button



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description       | Type     | Default     |
| -------- | --------- | ----------------- | -------- | ----------- |
| `label`  | `label`   | The buttons label | `string` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
