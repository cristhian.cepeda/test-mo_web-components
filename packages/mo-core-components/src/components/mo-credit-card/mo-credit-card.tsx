import { Component, h, Prop } from '@stencil/core';
import { FranchiseBackground } from './constants/mo-credit-card.constants';

@Component({
  tag: 'mo-credit-card',
  styleUrl: 'mo-credit-card.scss',
  shadow: false,
})
export class MoCreditCard {
  /**
   * The credit card number. It can be a string or a number.
   */
  @Prop() cardNumber: string | number;

  /**
   * The title of the credit card.
   */
  @Prop() cardTitle: string;

  /**
   * The title for the current balance section.
   */
  @Prop() currentBalanceTitle: string;

  /**
   * The value of the current balance.
   */
  @Prop() currentBalanceValue: string;

  /**
   * The URL of the franchise icon.
   */
  @Prop() franchiseIconUrl: string;

  /**
   * Determines whether the credit card is displayed in a small size.
   */
  @Prop() isSmall: boolean = false;

  /**
   * A custom CSS class for styling the element.
   */
  @Prop() customClass: string = '';

  /**
   * Inline style of the element.
   */
  @Prop() customStyle: any | null | undefined;

  /**
   * The gradient background for the credit card. Use 'FranchiseBackground' enum values.
   */
  @Prop() gradientBackground: FranchiseBackground = 'visa';

  /**
   * Specifies whether the credit card is disabled.
   */
  @Prop() isDisabled: boolean = false;

  private _maskCardNumber(cardNumber: number | string): string {
    if (!cardNumber) {
      return '**** **** **** ****';
    }
    const cardNumberString = cardNumber.toString();
    const maskedCardNumber = cardNumberString.replace(/\d(?=\d{4})/g, '*');
    const spacedCardNumber = maskedCardNumber.replace(/(.{4})/g, '$1 ');

    return spacedCardNumber.trim();
  }

  render() {
    return (
      <div
        class={`credit-card gradient-${this.gradientBackground} ${
          this.isSmall ? 'small' : ''
        } ${this.isDisabled ? 'disabled' : ''} ${this.customClass} `}
        style={this.customStyle}
      >
        <div class="credit-card__header">
          <p class="credit-card__header--number">
            {this._maskCardNumber(this.cardNumber)}
          </p>
          <p class="credit-card__header--title">{this.cardTitle}</p>
        </div>
        <div class="credit-card__footer">
          <div class="credit-card__footer--balance">
            <h3>{this.currentBalanceTitle}</h3>
            <p>{this.currentBalanceValue}</p>
          </div>
          <div class="credit-card__footer--franchise">
            <img src={this.franchiseIconUrl} alt="Franchise icon" />
          </div>
        </div>
      </div>
    );
  }
}
