export type FranchiseBackground =
  | 'visa'
  | 'visa-black'
  | 'mastercard'
  | 'mastercard-black'
  | 'american-express'
  | 'american-express-black'
  | 'discover'
  | 'discover-black'
  | 'diners-club'
  | 'diners-club-black';
