# mo-credit-card



<!-- Auto Generated Below -->


## Properties

| Property              | Attribute               | Description                                                                         | Type                                                                                                                                                                                       | Default     |
| --------------------- | ----------------------- | ----------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ----------- |
| `cardNumber`          | `card-number`           | The credit card number. It can be a string or a number.                             | `number \| string`                                                                                                                                                                         | `undefined` |
| `cardTitle`           | `card-title`            | The title of the credit card.                                                       | `string`                                                                                                                                                                                   | `undefined` |
| `currentBalanceTitle` | `current-balance-title` | The title for the current balance section.                                          | `string`                                                                                                                                                                                   | `undefined` |
| `currentBalanceValue` | `current-balance-value` | The value of the current balance.                                                   | `string`                                                                                                                                                                                   | `undefined` |
| `customClass`         | `custom-class`          | A custom CSS class for styling the element.                                         | `string`                                                                                                                                                                                   | `''`        |
| `customStyle`         | `custom-style`          | Inline style of the element.                                                        | `any`                                                                                                                                                                                      | `undefined` |
| `franchiseIconUrl`    | `franchise-icon-url`    | The URL of the franchise icon.                                                      | `string`                                                                                                                                                                                   | `undefined` |
| `gradientBackground`  | `gradient-background`   | The gradient background for the credit card. Use 'FranchiseBackground' enum values. | `"american-express" \| "american-express-black" \| "diners-club" \| "diners-club-black" \| "discover" \| "discover-black" \| "mastercard" \| "mastercard-black" \| "visa" \| "visa-black"` | `'visa'`    |
| `isDisabled`          | `is-disabled`           | Specifies whether the credit card is disabled.                                      | `boolean`                                                                                                                                                                                  | `false`     |
| `isSmall`             | `is-small`              | Determines whether the credit card is displayed in a small size.                    | `boolean`                                                                                                                                                                                  | `false`     |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
