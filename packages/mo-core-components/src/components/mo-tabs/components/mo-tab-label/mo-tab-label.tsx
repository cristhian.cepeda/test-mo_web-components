import {
  Component,
  Prop,
  Method,
  Event,
  EventEmitter,
  State,
  h,
} from '@stencil/core';
import { MoTabLabelData } from '../../constants/mo-tabs.interfaces';
import { generateId } from 'packages/mo-core-components/src/utils/utils';

@Component({
  tag: 'mo-tab-label',
  styleUrl: 'mo-tab-label.scss',
  shadow: false,
})
export class MoTabLabel {
  id: string = generateId();

  /**
   * Label name must match with the body name.
   */
  @Prop()
  name: string;

  /**
   * A custom CSS class for styling the element.
   */
  @Prop() customClass: string = '';

  /**
   * Inline style of the element.
   */
  @Prop() customStyle: any | null | undefined;
  /**
   * Indicates if tab is selected.
   */
  @State()
  isSelected: boolean = false;
  /**
   * Event emitted when the tab is selected.
   */
  @Event()
  selectTab: EventEmitter;

  /**
   * Method used from mo-tabs-group. Returns a promise that resolves to an object containing data about the MoTabLabel component.
   * @returns Promise<MoTabLabelData>
   */
  @Method()
  async getChild(): Promise<MoTabLabelData> {
    return Promise.resolve({
      select: this.select.bind(this),
      unselect: this.unselect.bind(this),
      name: this.name,
      id: this.id,
    });
  }

  unselect() {
    this.isSelected = false;
  }

  select() {
    this.isSelected = true;
  }

  onClick() {
    this.selectTab.emit(this.getChild());
  }

  render() {
    const classes = {
      'mo-tab-label': true,
      'mo-tab-label__selected': this.isSelected,
    };

    return [
      <div class={classes} onClick={this.onClick.bind(this)}>
        <slot />
      </div>,
    ];
  }
}
