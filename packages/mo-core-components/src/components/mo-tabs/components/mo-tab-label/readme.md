# mo-tab-label



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                 | Type     | Default     |
| ------------- | -------------- | ------------------------------------------- | -------- | ----------- |
| `customClass` | `custom-class` | A custom CSS class for styling the element. | `string` | `''`        |
| `customStyle` | `custom-style` | Inline style of the element.                | `any`    | `undefined` |
| `name`        | `name`         | Label name must match with the body name.   | `string` | `undefined` |


## Events

| Event       | Description                             | Type               |
| ----------- | --------------------------------------- | ------------------ |
| `selectTab` | Event emitted when the tab is selected. | `CustomEvent<any>` |


## Methods

### `getChild() => Promise<MoTabLabelData>`

Method used from mo-tabs-group. Returns a promise that resolves to an object containing data about the MoTabLabel component.

#### Returns

Type: `Promise<MoTabLabelData>`

Promise<MoTabLabelData>


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
