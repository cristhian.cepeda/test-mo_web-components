# mo-tabs-group



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                 | Type                            | Default     |
| ------------- | -------------- | ------------------------------------------- | ------------------------------- | ----------- |
| `customClass` | `custom-class` | A custom CSS class for styling the element. | `string`                        | `''`        |
| `customStyle` | `custom-style` | Inline style of the element.                | `any`                           | `undefined` |
| `tabsAlign`   | `tabs-align`   | Determine the alignment of the tabs.        | `"center" \| "left" \| "right"` | `'left'`    |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
