import { Component, h, Prop, Element, Listen } from '@stencil/core';
import {
  MoTabBodyData,
  MoTabLabelData,
  MoTabGroupData,
} from '../../constants/mo-tabs.interfaces';
import { TabsAlign } from '../../constants/mo-tabs.constants';

@Component({
  tag: 'mo-tabs',
  styleUrl: 'mo-tabs-group.scss',
  shadow: false,
})
export class MoTabsGroup {
  /**
   * Determine the alignment of the tabs.
   */
  @Prop() tabsAlign: TabsAlign = 'left';

  /**
   * A custom CSS class for styling the element.
   */
  @Prop() customClass: string = '';

  /**
   * Inline style of the element.
   */
  @Prop() customStyle: any | null | undefined;
  /**
   * The host element of the MoTabsGroup component.
   */
  @Element() host: HTMLElement;

  tabsLabel: MoTabLabelData[];
  tabsBody: MoTabBodyData[];
  tabGroup: MoTabGroupData[];

  async componentDidLoad() {
    await this.createGroup();
    const [group] = this.tabGroup;
    this.selectGroup(group);
  }

  @Listen('selectTab')
  onSelectedTab(event: CustomEvent) {
    event.detail.then((detail) => {
      const group = this.tabGroup.find((group) => group.label.id === detail.id);
      this.selectGroup(group);
    });
  }

  async createGroup() {
    const tabsLabelEl = Array.from(
      this.host.querySelectorAll('mo-tab-label')
    ) as any[];
    this.tabsLabel = await Promise.all(
      tabsLabelEl.map((el) => el.getChild().then((el) => el))
    );

    const tabsBodyEl = Array.from(
      this.host.querySelectorAll('mo-tab-body')
    ) as any[];
    this.tabsBody = await Promise.all(
      tabsBodyEl.map((el) => el.getChild().then((el) => el))
    );

    this.tabGroup = this.tabsLabel.map((label) => {
      const body = this.tabsBody.find((body) => body.name === label.name);
      return {
        label,
        body,
      };
    });
  }

  selectGroup(group: MoTabGroupData) {
    this.tabGroup.forEach((_) => {
      _.label.unselect();
      _.body.unselect();
    });

    group.label.select();
    group.body.select();
  }

  render() {
    return [
      <div class={`mo-tabs-label ${this.tabsAlign}`}>
        <slot name="label" />
      </div>,
      <div class="mo-tabs-body">
        <slot name="body" />
      </div>,
    ];
  }
}
