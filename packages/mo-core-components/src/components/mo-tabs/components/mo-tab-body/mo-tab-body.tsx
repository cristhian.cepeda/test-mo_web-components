import { Component, h, Prop, State, Method } from '@stencil/core';
import { MoTabBodyData } from '../../constants/mo-tabs.interfaces';
@Component({
  tag: 'mo-tab-body',
  styleUrl: 'mo-tab-body.scss',
  shadow: false,
})
export class MoTabBody {
  /**
   * Body name must match with the label name.
   */
  @Prop()
  name: string;
  /**
   * Indicates if tab is selected.
   */
  @State()
  isSelected: boolean = false;

  @Method()
  async getChild(): Promise<MoTabBodyData> {
    return Promise.resolve({
      select: this.select.bind(this),
      unselect: this.unselect.bind(this),
      name: this.name,
    });
  }

  unselect() {
    this.isSelected = false;
  }

  select() {
    this.isSelected = true;
  }

  render() {
    const classes = {
      'mo-tab-body': true,
      'mo-tab-body__selected': this.isSelected,
    };

    return (
      <div class={classes}>
        <slot />
      </div>
    );
  }
}
