# mo-tab-body



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                               | Type     | Default     |
| -------- | --------- | ----------------------------------------- | -------- | ----------- |
| `name`   | `name`    | Body name must match with the label name. | `string` | `undefined` |


## Methods

### `getChild() => Promise<MoTabBodyData>`



#### Returns

Type: `Promise<MoTabBodyData>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
