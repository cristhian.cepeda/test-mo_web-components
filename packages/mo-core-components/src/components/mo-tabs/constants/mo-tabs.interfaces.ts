export interface MoTabData {
  select: () => void;
  unselect: () => void;
  name: string;
}

export interface MoTabLabelData extends MoTabData {
  id: string;
}

export interface MoTabBodyData extends MoTabData {}

export interface MoTabGroupData {
  label: MoTabLabelData;
  body: MoTabBodyData;
}
