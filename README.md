# Mo Core WebComponents

This repository is a monorepo built using the NX workspace that contains a set of web components and related projects. The main purpose of this repository is to develop and test agnostic web components using Stencil, integrate them into an Angular application, and showcase their usage.

## Project Structure

The repository is structured as follows:

- **packages/mo-core-components:** This directory contains web components developed using Stencil. These components are designed to be agnostic and reusable across different frameworks.

- **packages/mo-core-components-angular:** This directory contains the Angular integration for the web components developed in the `mo-core-components` package. The components are rendered and used within an Angular environment.

- **packages/mo-angular-app:** This directory houses an Angular application that serves as a testing ground for the new components. The components developed in the `mo-core-components` package are consumed and showcased within this Angular app.

## Getting Started

Follow the steps below to get started with developing and testing the agnostic web components and their Angular integration.

### Prerequisites

- Node.js (version x.x.x)
- Angular CLI (version x.x.x)
- NX CLI (version 15.7.0)

### Installation

1. Clone this repository to your local machine.

2. Navigate to the root of the repository using the command line.

3. Install the required dependencies:

   ```
   npm install
   ```

### Building Agnostic Web Components

To build and test the web components developed with Stencil, you can use the following command:

```bash
nx build mo-core-components --watch
```

This command will compile and watch for changes in the `mo-core-components` package, allowing you to develop and see changes in real-time.

### Running the Angular App

To see how the agnostic web components are integrated into an Angular application, you can use the following command:

```bash
nx serve mo-angular-app
```

This command will start the development server for the Angular app located in the `mo-angular-app` package. You can access the app in your browser to interact with and test the components.

### Generating New Components

To generate a new component in the monorepo, you can use the following command:

```bash
nx g c component-name
```

Replace `component-name` with the desired name of your component. This command will scaffold the necessary files and configuration for your new component within the appropriate package.

---

For more information about NX, visit the [NX Documentation](https://nx.dev/) or read this article [Building React and Angular Component Libraries with Stencil and Nx](https://ionic.io/blog/building-react-and-angular-component-libraries-with-stencil-and-nx).
